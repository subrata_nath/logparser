package net.therap;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author subrata
 * @since 11/2/16
 */
public class ParsingUnit {

    private HashMap<String, LogObject> MappedLog = new HashMap<String, LogObject>();
    private final Integer halfDayHours = 12;
    private final Integer localTimeNewDayHours = 12;
    private final Integer worldTimeStartHours = 0;
    private final Integer range = 1;

    public ParsingUnit(String path) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(path));
        String line = null;
        while ((line = reader.readLine()) != null) {

            if (!MappedLog.containsKey(parseTime(line))) {
                if (parseGet(line) + parsePost(line) > 0) {
                    MappedLog.put(parseTime(line), new LogObject(parseTime(line), parseGet(line), parsePost(line),
                            parseURI(line), parseResponse(line)));
                }
            } else {
                updateLogObject(parseTime(line), line);
            }
        }
    }

    public void showOutputLog() {

        System.out.printf("%-20s %-15s %-16s %-14s \n", "Time", "GET/POST Count", "Unique URI Count",
                "Total Response Time");

        List<LogObject> outputList;
        if (LogParser.isSorted()) {
            outputList = new ArrayList<LogObject>(sortLogList());
        } else {
            outputList = new ArrayList<LogObject>(MappedLog.values());
        }

        for (Iterator<LogObject> it = outputList.iterator(); it.hasNext(); ) {
            LogObject next = (LogObject) it.next();
            String key = next.getTime();
            Integer startingRangeHours = Integer.parseInt(key);
            Integer endingRangeHours = Integer.parseInt(key) + range;
            String startingTimeSuffix = "am", endingTimeSuffix = "am";

            if (startingRangeHours >= halfDayHours) {
                startingTimeSuffix = "pm";
                if (startingRangeHours >= halfDayHours + 1) {
                    startingRangeHours -= halfDayHours;
                }
            } else {
                if (startingRangeHours == worldTimeStartHours) {
                    startingRangeHours = localTimeNewDayHours;
                }
            }

            if (endingRangeHours >= halfDayHours) {
                endingTimeSuffix = "pm";
                if (endingRangeHours >= halfDayHours + 1) {
                    endingRangeHours -= halfDayHours;
                }
            } else {
                if (endingRangeHours == worldTimeStartHours) {
                    endingRangeHours = localTimeNewDayHours;
                }
            }

            String keyToTimeRange = startingRangeHours + ".00" + startingTimeSuffix + "-" + endingRangeHours + ".00" +
                    endingTimeSuffix;
            System.out.printf("%-20s %-15s %-16d %-14s\n", keyToTimeRange, (next.getGetCount() + "/" +
                    next.getPostCount()), next.getUri().size(), next.getResponse() + " ms");
        }
    }


    public void updateLogObject(String time, String line) {
        if (parseGet(line) + parsePost(line) > 0) {
            HashSet<String> uri = new HashSet<String>(MappedLog.get(time).getUri());
            MappedLog.get(time).getUri().add(parseURI(line));
        }

        MappedLog.get(time).setGetCount(MappedLog.get(time).getGetCount() + parseGet(line));
        MappedLog.get(time).setPostCount(MappedLog.get(time).getPostCount() + parsePost(line));
        MappedLog.get(time).setResponse(MappedLog.get(time).getResponse() + parseResponse(line));
    }

    public String parseTime(String line) {
        Pattern p = Pattern.compile("(\\d+:\\d+:\\d+,(\\d)+)");
        Matcher m = p.matcher(line);
        String time = "";

        if (m.find()) {
            time = m.group().substring(0, 2);
        }

        return time;
    }


    public int parseGet(String line) {
        Pattern p = Pattern.compile("(\\sG,)");
        Matcher m = p.matcher(line);
        ArrayList<String> getlist = new ArrayList<String>();

        if (m.find()) {
            String get = m.group();
            getlist.add(get);
        }

        return getlist.size();
    }

    public int parsePost(String line) {
        Pattern p = Pattern.compile("(\\sP,)");
        Matcher m = p.matcher(line);
        ArrayList<String> postlist = new ArrayList<String>();

        if (m.find()) {
            String post = m.group();
            postlist.add(post);
        }

        return postlist.size();
    }


    public String parseURI(String line) {

        Pattern p = Pattern.compile("(URI=\\[.+\\])");
        Matcher m = p.matcher(line);
        String uri = "";

        if (m.find()) {
            uri = m.group();
        }

        return uri;
    }


    public int parseResponse(String line) {
        Pattern p = Pattern.compile("(time=[0-9]+)");
        Matcher m = p.matcher(line);
        int response = 0;

        if (m.find()) {
            response = Integer.parseInt(m.group().substring(5));
        }

        if (parseGet(line) + parsePost(line) > 0) {
            return response;
        } else {
            return 0;
        }
    }

    public Collection<LogObject> sortLogList() {
        Collection<LogObject> listToBeSorted = MappedLog.values();
        List<LogObject> sortedList = new ArrayList<LogObject>(listToBeSorted);
        Collections.sort(sortedList, new myComparator());

        return sortedList;
    }


    public class myComparator implements Comparator<LogObject> {
        @Override
        public int compare(LogObject s1, LogObject s2) {
            if ((s1.getGetCount() + s1.getPostCount()) > (s2.getGetCount() + s2.getPostCount())) {
                return -1;
            }
            return 1;
        }
    }
}

