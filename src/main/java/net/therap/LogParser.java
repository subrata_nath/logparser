package net.therap;

import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;

/**
 * @author subrata
 * @since 11/2/16
 */
public class LogParser {

    private static boolean sorted = false;

    public static boolean isSorted() {
        return sorted;
    }

    public static void setSorted(boolean sorted) {
        LogParser.sorted = sorted;
    }

    public static void main(String[] args) throws IOException {
        ParsingUnit parser;

        HashMap<String, LogObject> output;

        try {
            if (args.length == 1) {
                parser = new ParsingUnit(args[0]);
                sorted = false;
            } else if (args.length == 2 && args[1].contains("sort")) {
                System.out.println("Printing in sorted order");
                parser = new ParsingUnit(args[0]);
                sorted = true;
            } else {
                System.out.println("No arguements Found:( Plz pass the arguements. \n Insert the file Path here to " +
                        "view sorted output.");
                Scanner scan = new Scanner(System.in);
                String path = scan.nextLine();
                parser = new ParsingUnit(path);
                System.out.printf("Print in Sorted order? (y/n): ");
                String sortChoice = scan.nextLine();

                if (sortChoice.contains("y")) {
                    sorted = true;
                    System.out.println("Printing in sorted order");
                }
            }
            parser.showOutputLog();
        } catch (Exception err) {
            System.out.println("Something is wrong in your input");
        }
    }
}
