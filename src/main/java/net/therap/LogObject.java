package net.therap;

import java.util.HashSet;

/**
 * @author subrata
 * @since 11/2/16
 */
public class LogObject {
    private long getCount = 0;
    private long postCount = 0;
    private long response = 0;
    private String time = "";
    private HashSet uri = new HashSet();

    public LogObject(String time, int get, int post, String uri, int response) {
        this.time = time;
        this.getCount = get;
        this.postCount = post;
        this.response = response;
        this.uri.add(uri);
    }

    public long getGetCount() {
        return getCount;
    }

    public void setGetCount(long getCount) {
        this.getCount = getCount;
    }

    public long getPostCount() {
        return postCount;
    }

    public void setPostCount(long postCount) {
        this.postCount = postCount;
    }

    public long getResponse() {
        return response;
    }

    public void setResponse(long response) {
        this.response = response;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public HashSet getUri() {
        return uri;
    }

    public void setUri(HashSet uri) {
        this.uri = uri;
    }
}
